import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TollPage } from './toll';

@NgModule({
  declarations: [
    TollPage,
  ],
  imports: [
    IonicPageModule.forChild(TollPage),
  ],
})
export class TollPageModule {}
